import {
    FirehoseTransformationEvent,
    FirehoseTransformationResultRecord,
} from 'aws-lambda/trigger/kinesis-firehose-transformation'

export const handler = async (event: FirehoseTransformationEvent) => {
    const results: FirehoseTransformationResultRecord[] = event.records.map((record) => {
        const data = Buffer.from(record.data, 'base64').toString('utf8')
        const dataEvent = JSON.parse(data)
        const timestampAsDate = new Date(record.approximateArrivalTimestamp)
        const result: FirehoseTransformationResultRecord = {
            recordId: record.recordId,
            result: 'Ok',
            data: Buffer.from(JSON.stringify(dataEvent.detail), 'utf8').toString('base64'),
            metadata: {
                partitionKeys: {
                    custom_partition: dataEvent.detail.type,
                    year: timestampAsDate.getFullYear().toString(),
                    month: timestampAsDate.getMonth().toString(),
                    day: timestampAsDate.getDay().toString(),
                },
            },
        }

        return result
    })

    return {records: results}
}
