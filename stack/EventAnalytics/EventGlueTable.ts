import {Construct} from 'constructs'
import {CfnDatabase, CfnTable} from 'aws-cdk-lib/aws-glue'
import {Bucket} from 'aws-cdk-lib/aws-s3'

export interface MarketEventGlueDatabseProps {
    destinationBucket: Bucket,
    glueTableName: string,
    glueDatabaseName: string,
    glueDatabase: CfnDatabase,
}

export default (scope: Construct, props: MarketEventGlueDatabseProps): CfnTable => {

    const glueTable = new CfnTable(scope, 'IpponBlogDemoGlueTable', {
        databaseName: props.glueDatabaseName,
        catalogId: process.env.ACCOUNT_ID!,
        tableInput: {
            name: props.glueTableName,
            tableType: 'EXTERNAL_TABLE',
            partitionKeys: [
                {
                    name: 'year',
                    type: 'string',
                },
                {
                    name: 'month',
                    type: 'string',
                },
                {
                    name: 'day',
                    type: 'string',
                },
                {
                    name: 'custom_partition',
                    type: 'string',
                }
            ],
            storageDescriptor: {
                compressed: true,
                inputFormat: 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat',
                outputFormat: 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat',
                serdeInfo: {
                    serializationLibrary: 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe',
                },
                columns: [
                    {
                        name: 'name',
                        comment: 'Name of the item',
                        type: 'string'
                    },
                    {
                        name: 'price',
                        comment: 'Price of the item',
                        type: 'string'
                    },
                    {
                        name: 'quantity',
                        comment: 'Quantity of item',
                        type: 'string'
                    },
                ]
                ,
                location: props.destinationBucket.s3UrlForObject() + '/',
            },
        },
    })

    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.enabled', true)
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.year\\.type', 'integer')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.year\\.range', '2022,2050')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.year\\.digits', '4')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.month\\.type', 'integer')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.month\\.range', '1,12')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.month\\.digits', '2')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.day\\.type', 'integer')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.day\\.range', '1,31')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.day\\.digits', '2')
    glueTable.addPropertyOverride('TableInput.Parameters.projection\\.custom_partition\\.type', 'injected')

    glueTable.addPropertyOverride('TableInput.Parameters.storage\\.location\\.template', 's3://ippon-blog-demo-analytics-repository/year=${year}/month=${month}/day=${day}/custom_partition=${custom_partition}')

    glueTable.addDependsOn(props.glueDatabase)
    return glueTable
}