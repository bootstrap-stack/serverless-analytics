import {Construct} from 'constructs'
import {Bucket} from 'aws-cdk-lib/aws-s3'
import {CfnDeliveryStream} from 'aws-cdk-lib/aws-kinesisfirehose'
import {Effect, PolicyStatement, Role, ServicePrincipal} from 'aws-cdk-lib/aws-iam'
import {CfnDatabase} from 'aws-cdk-lib/aws-glue'
import {Lambda} from '../constructs/Lambda'

export interface MarketEventKinesisProps {
    destinationBucket: Bucket,
    glueDatabaseName: string,
    glueTableName: string,
    glueDatabase: CfnDatabase,
    lambdaProcessStreamEvent: Lambda,
}

export default (scope: Construct, props: MarketEventKinesisProps) => {
    const construct = new Construct(scope, 'BlogIpponDemoKinesisStream')


    // Create service roles for the delivery stream and destination.
    // These can be used for other purposes and granted access to different resources.
    // They must include the EventAnalytics Data Firehose service principal in their trust policies.
    // Two separate roles are shown below, but the same role can be used for both purposes.
    const deliveryStreamRole = new Role(
        construct, 'BlogIpponDemo@DeliveryStreamRole', {
            assumedBy: new ServicePrincipal('firehose.amazonaws.com'),
        },
    )

    deliveryStreamRole.addToPolicy(new PolicyStatement({
        resources: ['*'],
        actions: [
            'logs:CreateLogGroup',
            'logs:PutLogEvents',
            'logs:CreateLogStream',
        ],
    }))

    deliveryStreamRole.addToPolicy(new PolicyStatement({
        resources: [
            props.destinationBucket.bucketArn,
            props.destinationBucket.bucketArn + '/*',
        ],
        actions: [
            's3:AbortMultipartUpload',
            's3:GetBucketLocation',
            's3:GetObject',
            's3:ListBucket',
            's3:ListBucketMultipartUploads',
            's3:PutObject',
        ],
    }))

    deliveryStreamRole.addToPolicy(new PolicyStatement({
        effect: Effect.ALLOW,
        resources: [
            props.lambdaProcessStreamEvent.function.functionArn,
        ],
        actions: [
            'lambda:InvokeFunction',
            'lambda:GetFunctionConfiguration',
        ],
    }))

    deliveryStreamRole.addToPolicy(new PolicyStatement({
        effect: Effect.ALLOW,
        resources: [
            'arn:aws:glue:eu-west-1:' + process.env.ACCOUNT_ID + ':table/*',
            'arn:aws:glue:eu-west-1:' + process.env.ACCOUNT_ID + ':database/*',
            'arn:aws:glue:eu-west-1:' + process.env.ACCOUNT_ID + ':catalog',
        ],
        actions: [
            'glue:GetDatabase',
            'glue:GetTable',
            'glue:GetPartition*',
            'glue:GetTableVersions',
        ],
    }))

    const deliveryStream = new CfnDeliveryStream(construct, 'BlogIpponDemoDeliveryStream', {
        extendedS3DestinationConfiguration: {
            bucketArn: props.destinationBucket.bucketArn,
            roleArn: deliveryStreamRole.roleArn,
            bufferingHints: {
                intervalInSeconds: 60,
            },
            cloudWatchLoggingOptions: {
                enabled: true,
                logGroupName: 'kinesis-log-group',
                logStreamName: 'kinesis-logs',
            },
            prefix: 'year=!{timestamp:yyyy}/month=!{timestamp:MM}/day=!{timestamp:dd}/custom_partition=!{partitionKeyFromLambda:custom_partition}/',
            errorOutputPrefix: 'errors/!{firehose:error-output-type}/',
            processingConfiguration: {
                enabled: true,
                processors: [
                    {
                        type: 'Lambda',
                        parameters: [
                            {
                                parameterName: 'LambdaArn',
                                parameterValue: props.lambdaProcessStreamEvent.function.functionArn,
                            },
                        ],
                    },
                    {
                        type: 'AppendDelimiterToRecord',
                        parameters: [
                            {
                                parameterName: 'Delimiter',
                                parameterValue: '\\n',
                            },
                        ],
                    }],
            },
            dataFormatConversionConfiguration: {
                enabled: true,
                inputFormatConfiguration: {deserializer: {openXJsonSerDe: {}}},
                outputFormatConfiguration: {serializer: {parquetSerDe: {}}},
                schemaConfiguration: {
                    catalogId: props.glueDatabase.catalogId,
                    roleArn: deliveryStreamRole.roleArn,
                    databaseName: props.glueDatabaseName,
                    tableName: props.glueTableName,
                },
            },
            dynamicPartitioningConfiguration: {enabled: true, retryOptions: {durationInSeconds: 10}},
        },
    })
    // https://github.com/aws/aws-cdk/issues/7236
    deliveryStream.node.addDependency(deliveryStreamRole)
    return deliveryStream
}
