import {Construct} from 'constructs'
import EventDestinationBucket from './EventDestinationBucket'
import EventKinesisTransformationLambda from './EventKinesisTransformationLambda'
import EventKinesis from './EventKinesis'
import EventGlueTable from './EventGlueTable'
import EventGlueDatabase from "./EventDatabase/EventGlueDatabase";
import {EventBus, Rule} from "aws-cdk-lib/aws-events";
import {KinesisFirehoseStream} from "aws-cdk-lib/aws-events-targets";


export default (scope: Construct) => {
    const domain = 'EventAnalytics'
    const construct = new Construct(scope, domain)
    const glueTableName = 'demo-blog-ippon-glue-table'
    const glueDatabaseName = 'demo-blog-ippon-glue-database'

    const businessEventsBus = new EventBus(construct, 'DemoBlogIpponEventBus')
    // Here we create the S3 bucket we need to store the market event
    const destinationBucket = EventDestinationBucket(construct)

    const glueDatabase = EventGlueDatabase(construct, {glueDatabaseName})
    const glueTable = EventGlueTable(construct, {
        glueTableName,
        glueDatabase,
        glueDatabaseName: glueDatabaseName,
        destinationBucket,
    })

    // We need a lambda which process the event for the kinesis data firehose (else, we store data as described by
    // eventbridge) but we want to have business data, so we need to extract the market event stored in the message
    const lambdaProcessStreamEvent = EventKinesisTransformationLambda(construct,
        {lambdaName: 'ProcessStreamLambda'})

    const kinesisStream = EventKinesis(construct, {
        destinationBucket,
        glueDatabase: glueDatabase,
        glueDatabaseName: glueDatabaseName,
        glueTableName,
        lambdaProcessStreamEvent,
    })

    const invokeRule = new Rule(construct, 'ConsumeMarketEventRule', {
        eventBus: businessEventsBus,
        enabled: true,
        description: 'Rule trigger when an item is sold',
        eventPattern: {
            source: ['sales'],
            detailType: ['item.sold'],
        },
    })

    invokeRule.addTarget(new KinesisFirehoseStream(kinesisStream))
}