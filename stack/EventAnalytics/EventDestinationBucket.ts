import {Construct} from 'constructs'
import {Bucket} from 'aws-cdk-lib/aws-s3'

export default (scope: Construct): Bucket => {
    const construct = new Construct(scope, 'KinesisDestinationBucket')
    return new Bucket(construct, 'IpponBlodDemoBucket', {bucketName: 'ippon-blog-demo-analytics-repository'})
}