import { Construct } from 'constructs'
import { CfnDatabase } from 'aws-cdk-lib/aws-glue'

export interface MarketEventGlueDatabseProps {
    glueDatabaseName: string,
}

export default (scope: Construct, props: MarketEventGlueDatabseProps): CfnDatabase => {
    return new CfnDatabase(scope, 'glue-database-for-athena', {
        catalogId: process.env.ACCOUNT_ID!,
        databaseInput: {
            name: props.glueDatabaseName,
        },
    })
}