import {Construct} from 'constructs'
import {Alias} from 'aws-cdk-lib/aws-lambda'
import {LambdaDeploymentConfig, LambdaDeploymentGroup} from 'aws-cdk-lib/aws-codedeploy'
import LambdaConstruct, {Lambda} from '../constructs/Lambda'

export interface EventLambdaTransformationProps {
    lambdaName: string,
}

export default (scope: Construct, props: EventLambdaTransformationProps): Lambda => {
    const lambdaProcessStreamEvent = LambdaConstruct(scope, {
        domain: 'Market',
        name: props.lambdaName,
    })

    const version = lambdaProcessStreamEvent.function.currentVersion
    const alias = new Alias(scope, props.lambdaName + 'LambdaAlias', {
        aliasName: 'latest',
        version,
    })

    new LambdaDeploymentGroup(scope, props.lambdaName + 'DeploymentGroup', {
        alias,
        deploymentConfig: LambdaDeploymentConfig.ALL_AT_ONCE,
    })

    return lambdaProcessStreamEvent
}