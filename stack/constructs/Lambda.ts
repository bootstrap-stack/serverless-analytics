import {Construct} from 'constructs'
import {Code, Function, LayerVersion, Runtime} from 'aws-cdk-lib/aws-lambda'
import {IGrantable} from 'aws-cdk-lib/aws-iam'
import {RetentionDays} from 'aws-cdk-lib/aws-logs'
import {Duration, Tags} from 'aws-cdk-lib'

let depsLayer: LayerVersion | undefined = undefined

export interface LambdaProps {
    domain: string
    subdomain?: string
    name: string
    memorySize?: number
    timeout?: number
}

export interface Lambda extends IGrantable {
    function: Function
}

export default (scope: Construct, props: LambdaProps): Lambda => {
    const construct = new Construct(scope, props.name + 'Lambda')

    // Singleton
    if (!depsLayer) {
        depsLayer = new LayerVersion(construct, 'depsLayer', {
            compatibleRuntimes: [
                Runtime.NODEJS_16_X,
            ],
            code: Code.fromAsset('../stack/.dist/depsLayer'),
            description: 'Project dependencies',
        })
    }

    const lambda = new Function(construct, props.name, {
        runtime: Runtime.NODEJS_16_X,
        memorySize: props.memorySize || 1024,
        timeout: Duration.seconds(props.timeout || 3),
        handler: `ProcessStreamEventLambda.handler`,
        code: Code.fromAsset('../dist'),
        layers: [depsLayer],
        logRetention: RetentionDays.ONE_WEEK,
    })

    Tags.of(lambda).add('action', props.name)

    return {
        function: lambda,
        grantPrincipal: lambda.grantPrincipal,
    }
}

