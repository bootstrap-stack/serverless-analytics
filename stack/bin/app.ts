#!/usr/bin/env node
import * as cdk from "aws-cdk-lib";
import {Stack} from "aws-cdk-lib";
import EventAnalytics from "../EventAnalytics";

const app = new cdk.App();
const stack = new Stack(app, 'DemoBlogIppon')

EventAnalytics(stack)
