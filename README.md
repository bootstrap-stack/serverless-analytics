# Welcome to this serverless analytics bootstrap project

This is a base for further development regarding analytics serverless leveraging services like

* Glue
* EventBridge (we consume event from EB)
* Kinesis Data Firehose
* Athena
* Lambda function to transform events

This stack is written in typescript and is using AWS CDK in order to deploy the application.

You will only need to run these commands and set the ACCOUNT_ID (AWS account ID) environment variable
in order to make it work.

## Command needed

* `npm run build`
* `cd stack`                                          Change current directory to stack
* `npm install`                                       Install required npm packages
* `cp -r node_modules stack/.dist/depsLayer/nodejs`   Copy needed files into the .dist folder
* 
* `cdk bootstrap --profile {your_aws_profile_to_use}`                                   Commands needed to download cdk dependencies to your AWS account
* `cdk deploy --profile {your_aws_profile_to_use}`                                        deploy this stack to your AWS account in the default region
* `cdk destroy --profile {your_aws_profile_to_use}`                                       destroy this stack from your AWS account


## After deployment

You can now start playing with EventBridge and sending an event like : 
```javascript
{
    type: "WHOLESALE",
    name: "toothbrush",
    price: 9.99,
    quantity: 1
}            
```
in the source : "sales" and with the detail type : "item.sold".

You will need to wait 1/2 minutes for it to be sent to the S3 bucket. But you can then go to the lambda function used 
to transform the source event and see that it has been called.

You can then go to the Athena service in the AWS console, configure the S3 bucket destination for your query results and then start playing with the query editor.

A query like this should work (you need to replace some fields for the date to be today) :
```
select * from "demo-blog-ippon-glue-table" 
where year='CURRENT_YEAR' and month='CURRENT_MONTH' and day='CURRENT_DAY' and custom_partition='WHOLESALE'
```